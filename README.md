## Signaling Protocol:

### Namespaces
The server runs socket.io with Node.js.
For Text and Image messages,
each user owns 1 Socket connection to the server's "/message" namespace.
For Files, in each session,
the receiver and the sender own 1 Socket connection respectively
to a disposable "/file-[number]" namespace.
The connection is discarded after file transfer finishes.

### Events
There are 4 events in both the server and the clients:

SIGNAL, LEAVE, STATE, READY

Their parameters are slightly different between server and clients.

1. SIGNAL is used for events needed by the simple-peer module
2. LEAVE is broadcasted from the server to all the remaining clients each time a connection closes.
3. STATE is sent from the server to a newly connected client to inform it
the ids of all active clients.
4. READY is sent from the client after it handles STATE and is ready to connect with all active clients. It is then broadcasted to all other clients so that they can connect with the new client.

In the "/message" namespace, a clients needs to specify the target id of the SIGNAL event, since simple-peer requires what is essentially private messaging between peers.

The outcome of these events is to make sure that the client maintains a list of active peers.

### Example
Suppose the following events occur:
1. Server Starts and A arrives. Server sends STATE with an empty list to A. A responds with READY, but there is no peers to broadcast to.
2. B arrives. Server sends STATe with a list containing only A's id to B. B responds with READY. Server broadcasts READY with B's id to A.
3. C arrives. Server sends STATe with a list containing A and B's id to C. C responds with READY. The server broadcasts READY with C's id to A and B.
4. B leaves. Server notices and broadcast LEAVE with B's id to A and C.

### Optional things to do next

1. host a usable STUN server
- https://ourcodeworld.com/articles/read/1175/how-to-create-and-configure-your-own-stun-turn-server-with-coturn-in-ubuntu-18-04
- http://www.stunprotocol.org/

2. host mitm for StreamSaver
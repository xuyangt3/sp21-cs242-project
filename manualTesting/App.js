import logo from './logo.svg';
import './App.css';
import { useState } from 'react';

import {
  requestFile,
  uploadFile,
  startMessageConnection,
  sendMessage,
} from './peerConnectionModel';

function App() {
  const [selected, setSelected] = useState();
  function handler(e) {
    console.log(e);
    setSelected(e.target.files[0]);
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>

        <div>
          This is the test utility page
        </div>

        <input type="file" onChange={handler} />

        <button onClick={() => {
          requestFile(123321)
            .then(file => {
              console.log("creating file link")
              let url = URL.createObjectURL(file);
              let link = document.createElement('a');
              link.download = file.name;
              link.href = url;
              link.click();
              window.setTimeout(function () {
                URL.revokeObjectURL(url);
              }, 0);
            });
        }}>
          peer A
        </button>

        <button onClick={() => uploadFile(selected, 123321)}>
          Peer B Wait for Upload
        </button>
        <button onClick={()=>startMessageConnection(console.log)}>
          Join
        </button>
        <button onClick={()=>sendMessage("Hello, peer!")}>
          Send Message
        </button>
      </header>
    </div>
  );
}

export default App;

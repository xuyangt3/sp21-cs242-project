/* eslint-disable */

import Peer from 'simple-peer';
import { io } from 'socket.io-client';
import streamSaver from 'streamsaver';

/*
TODO Stream error handling (now it may hang)
*/

const wrtc = {
  RTCPeerConnection: window.RTCPeerConnection,
  RTCSessionDescription: window.RTCSessionDescription,
  RTCIceCandidate: window.RTCIceCandidate,
};

const SIGNAL = 'SIGNAL';

function createFilePeer(token, params) {
  console.log("create file peer", token, params);
  // session should be initiated by reciever
  const tempSocket = io(`http://127.0.0.1:2333/file-${token}`);
  const other = new Peer(params);

  // Warning: Call signal from other!!
  // Otherwise library code would use the wrong "this" pointer...
  // https://john-dugan.com/this-in-javascript
  tempSocket.on(SIGNAL, (data) => {
    console.log("file signal")
    other.signal(data);
  });
  other.on('signal', (data) => {
    tempSocket.emit(SIGNAL, data);
  });

  other.on('close', () => {
    console.log("File Peer Closed");
    tempSocket.disconnect();
  });

  return [other, tempSocket];
}

const SLICE_SIZE = 65536;

export function uploadFile(file, token) {
  console.log("upload file called");
  // session should be initiated by reciever
  const [receiver, tempSocket] = createFilePeer(token, { wrtc });

  function sendSlices(idx) {
    const slice = file.slice(idx * SLICE_SIZE, (idx + 1) * SLICE_SIZE);
    if (!slice.size) {
      console.log("write finished?")
      receiver.end();
      return;
    }
    const reader = new FileReader();
    reader.onload = () => {
      receiver.write(Buffer.from(reader.result),
        () => setTimeout(() => sendSlices(idx + 1)));
    };
    reader.readAsArrayBuffer(slice);
  }

  receiver.on('connect', () => {
    receiver.send(JSON.stringify({ name: file.name, size: file.size }));
    receiver.once('data', () => sendSlices(0));
  });
  return ()=>{tempSocket.disconnect();console.log("manual abort")};
}

export function requestFile(token) {
  const [sender, tempSocket] = createFilePeer(token, { initiator: true, wrtc });

  sender.once('data', (data) => {
    const meta = JSON.parse(data.toString());

    const fileOut = streamSaver.createWriteStream(meta.name, { size: meta.size });
    const writer = fileOut.getWriter();
    sender.on('data', (data) => {
      writer.write(data);
    });
    sender.on('end', () => { console.log("write ends"); writer.close() });
    sender.send('ack');

    return ()=>{tempSocket.disconnect();console.log("manual abort")};
  });
}

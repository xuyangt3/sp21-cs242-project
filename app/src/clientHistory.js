/* eslint-disable */

import {
  getHistory,
  getUserName,
} from "./peerModels";

const EPSILON = 0.1;  //some constant smaller than time granularity
let db;

// function afterAll(num, callback) {
//   let remain = num;
//   let counter = 0;
//   function getInstance(f) {
//     let called = false;
//     --remain;
//     if (remain<0) throw new Error("Register limit exceeded");
//     return (...args) => {
//       if (called) throw new Error("Called multiple times");
//       called = true;
//       const result = f(...args);
//       ++counter;
//       if (counter===num) callback();
//       return result;
//     }
//   }
//   return getInstance;
// }

/*
Contains all history before minT
*/
export function connectIDB(callback) {
  if (db) {
    db.close();
    db = undefined;
  }
  const userName = getUserName();
  const openRequest = indexedDB.open(`history-${userName}`);
  openRequest.onerror = () => console.error(openRequest.error);
  openRequest.onblocked = () => console.error("DB connection blocked");
  openRequest.onupgradeneeded = () => {
    console.log("Initialize db");
    const tempDB = openRequest.result;
    const entries = tempDB.createObjectStore(
      "entries", { autoIncrement: true });
    const meta = tempDB.createObjectStore("meta");

    entries.createIndex("time", "time", { unique: false });
    meta.transaction.oncomplete = () => {
      tempDB.transaction("meta", "readwrite")
        .objectStore("meta").put({
          minT:0,
          maxT:Date.now(),
          prevT:Date.now()
        },0);
    }
  };
  openRequest.onsuccess = () => {
    db = openRequest.result;
    cleanupCached(callback)
  };
}

function getCached(params, callback) {
  let data = [];
  let remain = params.max;
  const trans = db.transaction("entries", "readonly");
  trans.onerror = ()=>console.error(openRequest.error);
  const request = trans
    .objectStore("entries")
    .index("time")
    .openCursor(
      IDBKeyRange.bound(params.after, params.before),
      'prev'
    );
  request.onsuccess = () => {
    const cursor = request.result;
    if (cursor && remain!==0) {
      data.push(cursor.value);
      --remain;
      cursor.continue();
    } else {
      callback(data.reverse());
    }
  }
}

export function addCached(data, callback) {
  const trans = db.transaction(["meta", "entries"], "readwrite");
  trans.onerror = ()=>console.error(openRequest.error);
  trans.oncomplete = callback;

  getMetaOnTransaction(trans, (meta) => {
    meta.prevT = Date.now();
    setMetaOnTransaction(meta, trans, ()=>{
      const store = trans.objectStore("entries");
      data.map((v)=>store.add(v));
    });
  })
}

function cleanupCached(callback) {
  const trans = db.transaction(["meta", "entries"], "readwrite");
  trans.onerror = ()=>console.error(openRequest.error);
  trans.oncomplete = callback;

  getMetaOnTransaction(trans, ({minT, maxT, prevT})=>{
    let needsClean = true;
    if (maxT < minT) {
      minT = prevT;
      needsClean = false;
    }
    maxT = prevT = Date.now();
    console.log("cleanup needed:",needsClean);
    setMetaOnTransaction({minT, maxT, prevT}, trans, ()=>{
      if (!needsClean) return;
      const entries = trans.objectStore("entries");
      const request = entries.index("time")
        .openCursor(IDBKeyRange.lowerBound(minT),'prev');
      request.onsuccess = () => {
        const cursor = request.result;
        if (cursor) {
          entries.delete(cursor.primaryKey);
          cursor.continue();
        }
      }
    });
  });

}

function getMetaOnTransaction(trans, callback) {
  const request = trans.objectStore("meta").get(0);
  request.onsuccess = ()=>callback(request.result);
}

function setMetaOnTransaction(data, trans, callback) {
  const request = trans.objectStore("meta").put(data, 0);
  request.onsuccess = callback;
}

export function getHistoryCached(params, callback) {
  params = {after: 0, max: -1, ...params}
  //steps: get meta, fetch diff, put meta, add cached, get cached

  let apiCanceller, meta, limit;
  function apiCallback(data) {
    console.log("api called with limit:", limit);
    if (limit<0 || data.length<limit) {
      //exhausted api params range
      meta.maxT = params.after-EPSILON;
    } else if (data[0]) {
      //params range may contain more entries
      meta.maxT = data[0].time-EPSILON;
    }
    const trans = db.transaction(["meta", "entries"], "readwrite");
    trans.onerror = ()=>console.error(openRequest.error);
    trans.oncomplete = ()=>addCached(data, ()=>getCached(params, callback));
    setMetaOnTransaction(meta, trans);
  }

  
  const trans = db.transaction("meta", "readonly");
  trans.onerror = ()=>console.error(openRequest.error);
  getMetaOnTransaction(trans, (data) => {
    meta=data;
    const apiParams = {
      before: meta.maxT,
      after: Math.max(meta.minT, params.after),
      //may get more than needed
      max: params.before<meta.maxT? -1 : params.max,
    }
    if (apiParams.after <= apiParams.before) {
      limit = apiParams.max;
      apiCanceller = getHistory(apiParams, apiCallback);
    } else {
      //empty range
      getCached(params, callback)
    }
  });
  
  return ()=>apiCanceller;
}
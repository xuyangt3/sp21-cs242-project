/* eslint-disable */
import './App.css';
import React, { useEffect, useRef, useState } from 'react';
import {
  startMessageConnection,
  requestFile,
  uploadFile,
  getUserName,
  sendMessage,
} from './peerModels';
import { connectIDB, getHistoryCached, addCached } from './clientHistory';
import { createHash } from 'crypto';

function MainPagePeers({ peerList }) {
  return (
    <ul className="MainPage_tabItem">
      <div className="MainPage_title">Online Users</div>
      {peerList.map((v, i) => {
        return <li key={i} className="ListItem">{`[${v}]`}</li>
      })}
    </ul>
  );
}

function MessageEntry({ data, myID }) {
  console.log("to parse:",data);
  const  content = JSON.parse(data.data);
  return (
    <dl className={data.user === myID ?
      "Message__right" : "Message__left"}>
      <dt style={{ fontWeight: "bold" }}>{`[${data.user}]`}</dt>
      <dd>{content.text}</dd>
      <img className="Message_img" src={content.img} />
    </dl>);
}

function MainPageMessages({ messages, setMessages, since, setSince }) {
  const [refreshing, setRefreshing] = useState(false);
  function refresh(event) {
    if (refreshing) return;
    if (event.deltaY >= 0 || document.querySelector(".MainPage_messages")
      .scrollTop > 0) return;
    setRefreshing(true);
    getHistoryCached({ before: since, max: 2 }, (list) => {
      if (list[0]) {
        setSince(list[0].time - 0.1);
      }
      const myID = getUserName();
      setMessages((prev) =>
        list.map((data, i) => (
          <MessageEntry
            {...{ data, myID }}
            key={prev.length + list.length - i - 1} />
        )).concat(prev));
      setRefreshing(false);
    });
  }
  return (
    <ol className="MainPage_messages" onWheel={refresh}>
      <li
        className="Message__left"
        style={{
          fontStyle: 'italic',
          fontWeight: "bold",
          display: refreshing || "none"
        }}>
        Loading...
      </li>
      {messages}
    </ol>
  );
}

function FilesToShare({ toShare, setToShare }) {
  return (
    <ul className="MainPage_tabItem">
      <div className="MainPage_title">Files to Share</div>
      {toShare.map((entry, i) => (
        <li className="ListItem">
          <div>{entry.file.name}</div>
          <button onClick={() => setToShare(
            (prev) => prev.filter(({ token }) => token !== entry.token))}>
            Remove
          </button>
        </li>))}
    </ul>
  )
}

function FilesShared({ shared }) {
  console.log("shared:", shared);
  return (
    <ul className="MainPage_tabItem">
      <div className="MainPage_title">Shared Files</div>
      {shared.map(({ user, files }) => [
        <div className="ListItem">{user}</div>
      ].concat(files.map(({ fileName, token }) => (
        <li className="ListItem">
          <div>{fileName}</div>
          <button onClick={() => requestFile(token)}>
            Download
          </button>
        </li>)).flat()))}
    </ul>);
}

function UserNameSetting({ join, userName }) {
  const [errMsg, setErrMsg] = useState(undefined);
  function handler() {
    const tempUserName = document.querySelector("#user-name").value;

    join(tempUserName)
      .then(() => setErrMsg(undefined))
      .catch(err => setErrMsg(err.toString()));
  }
  return (
    <div className="SettingItem">
      <label htmlFor="user-name">User Name</label>
      <input
        className="SettingItem_input"
        id="user-name"
        type="text"
        title="Your User Name"
        defaultValue={userName}
      />
      <button onClick={handler}>Join</button>
      <div>{errMsg}</div>
    </div>);
}

const MESSAGE = "MESSAGE";
const FILE_INFO = "FILE_INFO";

function App() {
  const [userName, setUserName] = useState(undefined);
  const [peerList, setPeerList] = useState([]);
  const [since, setSince] = useState(undefined);
  const [messages, setMessages] = useState([]);
  const [toShare, setToShare] = useState([]);
  const [shared, setShared] = useState([]);
  const imgRef = useRef("");

  useEffect(() => console.log("shared updated:", shared), [shared]);

  function receive(data) {
    if (data.type === MESSAGE) {
      addCached([data]);
      setMessages((prev) => prev.concat(
        <MessageEntry
          data={data} myID={getUserName()}
          key={prev.length} />));
    } else if (data.type === FILE_INFO) {
      setShared((prev) => {
        let tempShared = prev.filter(({ user }) => user !== data.user);
        if (data.data.length) {
          tempShared = tempShared.concat(
            { user: data.user, files: data.data });
        }
        return tempShared;
      })
    }
  }

  function send() {
    console.log("img is",imgRef.current);
    const area = document.querySelector("footer > textarea");
    const imgInput = document.querySelector("#choose-img");
    const data = {
      data: JSON.stringify({text: area.value, img: imgRef.current}),
      type: MESSAGE
    };
    sendMessage(data, true);
    receive({ ...data, user: getUserName() });
    area.value = "";
    imgRef.current = "";
    imgInput.value = "";
  }

  function join(newUserName) {
    return startMessageConnection(newUserName, receive,
      (newPeerList) => setPeerList((prev) => {
        //Simulate remove shared messasge for peers that has left
        const newPeerSet = new Set(newPeerList);
        prev.filter((peer) => !newPeerSet.has(peer))
          .forEach((peer) => receive({
            data: [], user: peer, type: FILE_INFO }));
        return newPeerList;
      }))
      .then(() => {
        setUserName(newUserName);
        setMessages([]);
        connectIDB(() => setSince(Date.now()));
        changeVisibility();
      });
  }

  function changeVisibility() {
    const style = document.querySelector(".Settings").style;
    if (style.display === "none") {
      style.display = "flex";
    } else {
      style.display = "none";
    }
  }

  useEffect(() => {
    sendMessage({ data: toShare.map(({ file, token }) => ({ fileName: file.name, token })), type: FILE_INFO }, false);
  }, [toShare])

  function addToShare(event) {
    console.log("on change triggered");
    const file = event.target.files[0];
    const timeBuffer = Buffer.alloc(6);
    timeBuffer.writeIntBE(Date.now(), 0, 6);

    const token = createHash("md5").update(timeBuffer).update(file.name).digest("base64");
    setToShare((prev) => prev.concat({ file, token }));
    uploadFile(file, token);
    event.target.value = "";
  }

  function readImg(event) {
    const file = event.target.files[0];
    if (!file) {
      console.log("file empty")
      imgRef.current = "";
      return;
    }
    const reader = new FileReader();
    reader.onloadend = ()=>imgRef.current=reader.result;
    reader.readAsDataURL(file);
  }

  return (
    <div className="App">
      <div className="Settings">
        <button className="CloseButton" disabled={userName===undefined}
          onClick={changeVisibility}>
          Close Settings
        </button>
        <UserNameSetting {...{ join, userName }} />
        <div className="Overlay" />
      </div>
      <div className="MainPage">
        <div className="MainPage_tab">
          <MainPagePeers peerList={peerList} />
          <FilesShared shared={shared} />
          <FilesToShare {...{ toShare, setToShare }} />
        </div>
        <MainPageMessages {...{ messages, setMessages, since, setSince }} />
      </div>
      <footer className="Bar">
        <button type="button" className="Bar_item" onClick={changeVisibility}>
          Settings
        </button>
        <input style={{display:"none"}}
          type="file" id="share-file" onChange={addToShare} />
        <input style={{display:"none"}} accept="image/*"
          type="file" id="choose-img" onChange={readImg} />
        <button type="button" className="Bar_item"
          onClick={()=>document.querySelector("#share-file").click()}>
          Share File
        </button>
        <button type="button" className="Bar_item"
          onClick={()=>document.querySelector("#choose-img").click()}>
          Choose Image
        </button>
        <textarea
          className="Bar_textArea"
          type="text"
          placeholder="Enter Text Here"
        />
        <button type="button" className="Bar_item" onClick={send}>
          Send
        </button>
      </footer>
    </div>
  );
}

export default App;

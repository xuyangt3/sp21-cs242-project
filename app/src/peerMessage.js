/* eslint-disable */
import Peer from 'simple-peer';
import { io } from 'socket.io-client';

const wrtc = {
  RTCPeerConnection: window.RTCPeerConnection,
  RTCSessionDescription: window.RTCSessionDescription,
  RTCIceCandidate: window.RTCIceCandidate,
};

const POST = 'POST';
const GET = 'GET';
const DATA = 'DATA';
const DONE = 'DONE';

const SIGNAL = 'SIGNAL';
const LEAVE = 'LEAVE';
const STATE = 'STATE';
const READY = 'READY';

let mySocket;
let userName;
let peerDict;
let receiveMessageCallback;
let peerUpdateCallback;

function addPeer(remoteID, params) {
  console.log("add peer",remoteID);

  const peer = new Peer(params);

  peer.on('signal', (data) => {
    mySocket.emit(SIGNAL, remoteID, data);
  });

  peer.on('error', (e) => {
    console.error(e);
  });

  // todo bug may occur
  peer.on('data', (data) => {
    console.log("received message:",data);
    console.log("parsed to:",{...JSON.parse(data), user: remoteID});
    receiveMessageCallback({...JSON.parse(data), user: remoteID});
  });

  peerDict[remoteID] = peer;
  return peer;
}

function removePeer(remoteID) {
  console.log("request to remove",remoteID);
  if (!(remoteID in peerDict)) return false;
  console.log("remove peer",remoteID);
  peerDict[remoteID].end();
  delete peerDict[remoteID];
  peerUpdateCallback(Object.keys(peerDict));
  return true;
}

export function getUserName() {
  if (userName===undefined) throw new Error("peerModel not ready");
  return userName;
}

export function startMessageConnection(myUserName, onReceiveMessage, onPeerUpdate) {
  return new Promise((resolve, reject)=>{
    if (mySocket) mySocket.disconnect();
    mySocket = io('http://127.0.0.1:2333/message',{auth:{user:myUserName}});
    userName = undefined;
    peerDict = {};
    receiveMessageCallback = onReceiveMessage;
    peerUpdateCallback = onPeerUpdate;

    mySocket.on("connect_error", (err)=>{
      mySocket.disconnect();
      reject(err);
    });

    mySocket.on(SIGNAL, (remoteID, data) => {
      if (remoteID in peerDict) {
        peerDict[remoteID].signal(data);
      } else {
        console.error("remoteID does not exist", remoteID);
      }
    });

    mySocket.on(STATE, (ids) => {
      userName = myUserName;
      console.log("init peers");
      console.log("ids:",ids);
      ids.forEach(remoteID => {
        console.log("remote id:",remoteID);
        if (remoteID !== userName) {
          addPeer(remoteID, { wrtc });
        }
      });
      peerUpdateCallback(Object.keys(peerDict));
      mySocket.emit(READY);
      resolve();
    });

    // We choose the elder connections as initiators
    mySocket.on(READY, remoteID => {
      addPeer(remoteID, {initiator: true, wrtc});
      peerUpdateCallback(Object.keys(peerDict));
    });

    mySocket.on(LEAVE, removePeer);
  });
}

/**
 * Send the message to all peers.
 * @param {Object} message history entry 
 * @param {boolean} ccServer whether to send a copy to server
 */
export function sendMessage(message, ccServer=true) {
  console.log("to send:",message);
  if (ccServer) {
    mySocket.emit(POST, message);
  }
  for (const remoteID in peerDict) {
    peerDict[remoteID].send(JSON.stringify(message));
  }
}

export function getHistory(params, callback) {
  const tempSocket = io('http://127.0.0.1:2333/history');
  const result = [];

  let called = false;
  function onFinish() {
    if (called) return;
    called = true;
    callback(result.reverse());
    tempSocket.disconnect();
  }

  tempSocket.on(DATA, (data)=>result.push(data));
  tempSocket.on(DONE, onFinish);
  
  tempSocket.emit(GET, params);
  //call this function to cancel
  return onFinish;
}
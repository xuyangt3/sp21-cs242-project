const history = require('./history')('history.db');
const io = require('socket.io')({
  cors: {
    origin: '*',
    methods: ['GET', 'POST'],
  },
});

//User can copy the message sent and POST to the server.
const POST = 'POST';

const SIGNAL = 'SIGNAL';
const LEAVE = 'LEAVE';
const STATE = 'STATE';
const READY = 'READY';

const userNameToId = {};

/* Assume the socket is the user it claims to be.
Its identity can be validated when signing is used. */
io.of('/message').use((socket, next)=>{
  if (socket.handshake.auth.user in userNameToId) {
    console.log("reject id:", socket.id)
    next(new Error("user already exists"));
  } else {
    next();
  }
})

io.of('/message').on('connection', async (socket) => {
  const userName = socket.handshake.auth.user;
  userNameToId[userName] = socket.id;

  socket.on(POST, (data) => history.addEntry({...data, user:userName}));

  socket.on(SIGNAL, (peerUserName, data) => {
    const id = userNameToId[peerUserName];
    socket.to(id).emit(SIGNAL, userName, data);
  });

  socket.on(READY, () => {
    console.log("READY:", socket.id);
    socket.broadcast.emit(READY, userName);
  });

  socket.on('disconnect', () => {
    console.log("disconnect:", socket.id);
    socket.broadcast.emit(LEAVE, userName);
    delete userNameToId[userName];
  });

  console.log("connection:",socket.id);
  const ids = await io.of('/message').allSockets();
  console.log('current clients:', ids);
  socket.emit(STATE, Object.keys(userNameToId));
});

//Simple signals for sending history data
/* Client request history by sending GET with params. */
const GET = 'GET';
/* Server respond DATA with data for each history entry
respond DONE when all entries are sent */
const DATA = 'DATA';
const DONE = 'DONE';

io.of('/history').on('connection', (socket) => {
  socket.on(GET, (request) => history.getEntries(
    (err, entry)=>socket.emit(DATA, entry),
    ()=>socket.emit(DONE),
    request));
})

// This namespace should only have 2 connections
io.of(/^\/file-[A-Za-z0-9+/]*={0,3}$/).on('connection', (socket) => {
  socket.on(SIGNAL, (data) => {
    socket.broadcast.emit(SIGNAL, data);
  });
  (async ()=>{
    const ids = await socket.nsp.allSockets();
    console.log('current Sockets:', ids);
  })();
  console.log("nsp:",socket.nsp)
  console.log(`File Peer ${socket.id} joined`);
  socket.on("disconnect", ()=>console.log(`File Peer ${socket.id} disconnected`));
});

const port = 2333;
io.listen(port);
console.log(`Listening on port ${port}...`);

const sqlite3 = require('sqlite3').verbose();

//Expire in 30 days
const EXPIRATION_INTERVAL = 1000 * 60 * 60 * 24 * 30;
/*
Cleanup every day. setInterval has
a limit of 2^31-1 milliseconds (~24 days).
*/
const CLEANUP_INTERVAL = 1000 * 60 * 60 * 24;

let db;

function addEntry({user:$user, type:$type, data:$data}, callback=getLogger()) {
  db.serialize(()=>{
    db.run(`INSERT INTO history (user, type, data)
      VALUES ($user, $type, $data)`, {$user, $type, $data}, callback);
  });
}

function getEntries(callback, complete=getLogger(),
    {before:$before, after:$after = 0, max:$max = -1}) {
  db.serialize(()=>{
    db.each(`SELECT * FROM history WHERE time BETWEEN $after AND $before
      ORDER BY time DESC LIMIT $max`,
      {$before, $after, $max}, callback, complete);
  });
}

function cleanup(callback=getLogger("Cleanup complete")) {
  db.serialize(() => {
    db.run("DELETE FROM history WHERE time+$duration < $current",
      { $duration:EXPIRATION_INTERVAL, $current: Date.now() },
      callback);
  });
}

function getLogger(successMessage) {
  return (err) => {
    if (err) {
      console.error(err);
    } else if (successMessage) {
      console.log(successMessage);
    }
  };
}

module.exports = (dbPath)=> {
  db = new sqlite3.Database(dbPath);
  
  setInterval(cleanup, CLEANUP_INTERVAL);

  let closed = false;
  function close() {
    if (closed) return;
    process.removeListener('exit', close);
    console.log("Closing database...");
    db.close();
  }
  process.on('exit', close);
  db.serialize(() => {
    const log = getLogger();
    db.exec('PRAGMA encoding="UTF-8"', log);
    db.exec(`CREATE TABLE IF NOT EXISTS history (
          user TEXT NOT NULL,
          time INTEGER DEFAULT (strftime('%s','now') ||
            substr(strftime('%f','now'),4)),
          type TEXT,
          data TEXT
      );`, log);
    db.exec("CREATE INDEX IF NOT EXISTS idx ON history (time);", log);
    cleanup();
  });
  
  return {
    addEntry,
    getEntries,
    cleanup,
    close,
  }
}
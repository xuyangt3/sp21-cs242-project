//Forget about jest! It doesn't work with timeout.
const assert = require('assert');
const { copyFileSync } = require('fs');
const history = require('./history');

const dbPath = "test.db";

let failCount = 0;

function init() {
  try {
    copyFileSync("dataSample.db", dbPath);
    console.log("sample db copied");
    return history(dbPath);
  } catch (e) {
    console.log(e.message);
    if (e.code !== "ENOENT") return -1;
  };
}

function count(fn) {
  try {
    fn();
  } catch (e) {
    console.log(e);
    ++failCount;
  }
}

const expected = [
  {
    user: 'bob',
    time: 1618335257880,
    type: 'blob',
    data: '90284982735982375928375laskjdflksajf'
  },
  {
    user: 'alice',
    time: 1618335218572,
    type: 'text',
    data: 'lasjkdflasjdflakjsdf'
  }
];

function testGetEntries(expected, params, message) {
  const storage = init();
  let result = [];
  return new Promise((res) =>
    storage.getEntries(
      (err, entry)=>count(()=>{
        if (err) throw err;
        result.push(entry);
      }), (err)=>count(()=>{
        if (err) throw err;
        storage.close();
        res();
        assert.deepStrictEqual(result, expected, message);
        console.log(`${message} success`);
      }), params
  ));
}

function testAddEntry(params, message) {
  const storage = init();
  //newer on top
  return new Promise((res) => storage.addEntry(params, () => {
    storage.getEntries((err, entry)=>count(()=>{
      if (err) throw err;
      storage.close();
      res();
      delete entry.time;
      assert.deepStrictEqual(entry, params, message);
      console.log(`${message} success`);
    }), undefined, {before: Date.now(), max:1});
  }));
}

function testCleanup(message) {
  const storage = init();
  const orig = Date.now;
  Date.now = ()=>1618335218572 + 1000*60*60*24*30+1;
  return new Promise((res)=>{
    storage.cleanup(()=>{
      storage.getEntries((err, entry)=>{
        if (err) throw err;
        storage.close();
        res();
        assert.deepStrictEqual(entry, expected[0], message);
        console.log(`${message} success`);
      }, ()=>{Date.now=orig}, {before: Date.now()});
    });
  })
}

testGetEntries(expected, {before: Date.now()}, "Get all")
  .finally(()=>testGetEntries([expected[1]],
    {before: 1618335218572}, "Get before"))
  .finally(()=>testGetEntries([expected[0]],
    {before: Date.now(), after:1618335257880}, "Get after"))
  .finally(()=>testGetEntries([expected[0]],
   {before: Date.now(), max:1}, "Get limited"))
  .finally(()=>testAddEntry(
    {user:"Carl",type:"img",data:"asdflkajwoiewer"}, "Add one"))
  .finally(()=>testCleanup("Delete expired"))
  .finally(()=>setTimeout(()=>process.exit(failCount)));

chat room
====

Xuyang Tang (xuyangt3)

Moderator: Akhil Isanaka (isanaka2)

## Project Purpose ##

Create a chat room for a small number (dozens) of people.

## Project Motivation ##

Have a convenient tool to communicate with friends and (especially) share large files, with more privacy.

## Technical Specification ##

* Platform: Cross Platform

* Language: JS

* Frontend Tool: React Native

* Backend Tools:

    Node.js (up to change... Use whatever backend that works best with webRTC)

    MongoDB (or SQLite)

* IDE: Visual Studio Code

* Node: use webRTC

## Style ##

CS 242 default (Airbnb)

## Functional Specification ##

* use encryption of some sort to keep the data private among members

* support message types:

    image: small images that are compressed and saved on the server. gif support is optional
    
    text: text messages saved on the server
    
    file link: members can put up a list of files to be shared p2p

* Can retrieve message history from server

* History data are deleted from the server (expire) after a period of time.

* The final implementation may choose one of the following ways to perform authentication:

    One server-wide password
    
    White list by manually entering public keys at the server
    
    Let the user store a list of trusted peers
    
    (Most tricky) Log in before entering the chat room

* Notification of updates (new messages, new files)

## UI Sketch ##

![UI Sketch](./proposedUI.png)

## Proposed Timeline ##

* Week 1:

    Set up the server for p2p file sharing. i.e. write a WebRTC "signaling server" and design/choose an appropriate protocol to do signaling.
    
    Write the client-side models to send/receive files, text, and images.
    
    https://docs.google.com/spreadsheets/d/156Zi1-jziieoCUFY3Swha4NPZULHWwYnlI1vsGbyUsQ/edit?usp=sharing (updated)
    
* Week 2:

    Write the UI for the message page. For this checkpoint, the UI only needs to interact with text messages.

    Add database to the server to store history.
    
    Complete the server's functions for text messaging, which is essentially supporting text message history.
    
    https://docs.google.com/spreadsheets/d/1DJBU60IZUvF1qIh_hvDnNBZ7ebE4B_77mW7AyJ9YesE/edit?usp=sharing (updated)

* Week 3:

    Support persistent data storage at the client side, including file links and message history.

    Write the interactive UI for file sharing and sending/displaying image.
    
    https://docs.google.com/spreadsheets/d/1TUhFLZngXwjNxJi9Ei860knJRGU_gCd5iKjsbi653sk/edit?usp=sharing (updated)
    
* Week 4:

    Finish UI and server features. The user should be able to fetch message history from the server's database. The server should store (compressed) history images, if that hasn't been done.

    Add authentication (and encryption if applicable)

    If there is still time, add some interesting features such as displaying user status (online/offline/afk), storing image macro locally,
    or improving file transfer reliability.

    https://docs.google.com/spreadsheets/d/1aoxq3GMKbE-YRZ_32nNIha7mCXIKK6rDvERihrRWRo0/edit?usp=sharing (updated)
